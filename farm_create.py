import json
from state import state_load, state_save


STATE_FILE = "state.json"

# create empty farm
state = {
    "farm": {
        "health": 1, # in percentage (we start with healthy farm)
        "ripeness": 0, # in percentage (start of the season, leaves are not ripe at all)
        "age": 0, # in weeks
        "humidity": 0.75, # percentage
        "sunlight": 31.5, # 4.5 hours/day (average for the location, this affects humidity variable)
        "rainfall": 50, # 2590 mm/year (average for the location, this affects humidity variable)
        "SQI": 0.75, # soil quality index (this number can stay be unchanged for one season/year)
        "infestation": 0, # percentage (1 means pests are all over the farm)
    }
}

# dump state into json file using function from state.py
state_save(state)

print(f"Created empty farm in {STATE_FILE}.")
