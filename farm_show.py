import json
from state import *

# load data from json file as "state" using function from state.py
state = state_load()

# using function from state.py
state_print(state)
