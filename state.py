import json

STATE_FILE = "state.json"

def state_load():
    with open(STATE_FILE) as fp:
            return json.load(fp)

def state_save(state):
    with open(STATE_FILE, "w") as fp:
            json.dump(state, fp)

def state_print(state):
    print(f"""\
            Week: {state['farm']['age']},
            Sunlight: {state['farm']['sunlight']} hrs,
            Rained: {state['farm']['rainfall']} mm,
            Humidity: {round(100 * state['farm']['humidity'])} %,
            SQI: {round(100 * state['farm']['SQI'])} %,
            Infestation: {round(100 * state['farm']['infestation'])} %
            Ripeness: {round(100 * state['farm']['ripeness'])} %
            Health of the farm: {round(100 * state['farm']['health'])} %\
                    """)

