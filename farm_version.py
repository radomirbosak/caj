__version__ = "0.1"

def show_version():
        print(f"Program Version: {__version__}")

if __name__ == "__main__":
        show_version()
