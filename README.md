# Čaj

Tea plantation simulation

# Setup

## Development environment setup

Do this in project directory after cloning.

```shell
# create virtual environment called ".venv"
$ python3 -m venv .venv

# activate it (may differ in windows)
$ source .venv/bin/activate

# check if it is activated
$ echo $VIRTUAL_ENV
/path/to/caj/.venv

# these should now point to venv paths
$ which python
/path/to/caj/.venv/bin/python
$ which pip
/path/to/caj/.venv/bin/pip
```

Install dependencies and check if they are functional

```shell
$ pip install -r requirements.txt
$ python3 check_venv.py
requests depdendency is imported from /path/to/caj/.venv/lib/python3.11/site-packages/requests/__init__.py .
```

# Usage

```shell
# Create new farm
$ python3 farm_create.py

# Show farm status
$ python3 farm_show.py

# Simulate farm
$ python3 farm_grow.py

# Delete farm status
$ rm state.json
```
