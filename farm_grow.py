#this script changes the state of the farm simulation in 1 time step represented by 1 week 
import json
import random
from state import *

# load data from json file as "state" using function from state.py
state = state_load()

state["farm"]["age"] += 1
rain = random.randint(0, 100)
state["farm"]["rainfall"] = rain
sun = random.randint(0, 63)
state["farm"]["sunlight"] = sun
#humidity will be calculated based on average humidity for the location, rainfall volume and sunlight volume during this step(week)
#state["farm"]["humidity"] = 

# dump state into json file using function from state.py
state_save(state)

# using function from state.py
state_print(state)
